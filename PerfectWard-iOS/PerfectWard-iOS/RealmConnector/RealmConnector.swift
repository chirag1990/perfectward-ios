//
//  RealmConnector.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import Foundation
import RealmSwift

class RealmConnector {
    
    public static var shared = RealmConnector()
    
    public var realm: Realm {
        return try! Realm()
    }
    
    public func writeQuestionnaireToRealm(with id: String, questionnaireId: String) {
        let questionnaireData: RealmQuestionnaireModel = RealmQuestionnaireModel()
        
        questionnaireData.id = id
        questionnaireData.questionnaire_global_id = questionnaireId
        questionnaireData.isSynced = false
        questionnaireData.isCompleted = false
        questionnaireData.isSaved = true
        questionnaireData.savedDate = Date()
        
        do {
            try realm.write {
                realm.add(questionnaireData)
            }
        } catch {
            print("Error adding data to questionnaireData")
        }
    }
    
    public func fetchSavedQuestionnaireFromRealm(questionnaireId: String) -> Results<RealmQuestionnaireModel> {
        return realm.objects(RealmQuestionnaireModel.self).filter("isSaved == TRUE AND questionnaire_global_id == %@", questionnaireId)
    }
    
    public func writeAnswersForQuestionnaires(questionnaireId: String, primaryKey: String, answerModel : AnswerModel) {
        
        let answerData: RealmQuestionAnswerModel = RealmQuestionAnswerModel()
        answerData.id = primaryKey
        answerData.questionnaire_global_id = questionnaireId
        answerData.isSaved = true
        answerData.question_global_id = answerModel.question_global_id ?? ""
        answerData.answer = answerModel.answer ?? ""
        answerData.question_type = answerModel.questionType ?? ""
        
        
        do {
            try realm.write {
                realm.add(answerData, update: .all)
            }
        } catch {
            print("Error adding data to answerData")
        }
    }
    
    
    public func fetchSavedAnswer(questionnaireId: String) -> Results<RealmQuestionAnswerModel> {
        return realm.objects(RealmQuestionAnswerModel.self).filter("isSaved == TRUE AND questionnaire_global_id == %@", questionnaireId)
    }
    
    public func fectchSavedAnswerForQuestionGlobalId(questionGlobalId: String) -> Results<RealmQuestionAnswerModel> {
        return realm.objects(RealmQuestionAnswerModel.self).filter("isSaved == TRUE AND question_global_id == %@", questionGlobalId)
    }
}
