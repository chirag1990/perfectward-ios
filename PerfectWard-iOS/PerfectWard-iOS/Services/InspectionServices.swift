//
//  InspectionServices.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 20/05/2021.
//

import Foundation

typealias InspectionCompletionHandler = (_ users: InspectionModel?, _ status: Bool, _ message: String) -> Void

class InspectionServices {
    
    public static let shared = InspectionServices()
    var inspections: InspectionModel?
    
    func getInspections(completionHandler: @escaping InspectionCompletionHandler) {
        
        guard let path = Bundle.main.path(forResource: "Inspections", ofType: "json") else {
            print("Users JSON File not found")
            return
        }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let data = try Data(contentsOf: url)
            inspections = try JSONDecoder().decode(InspectionModel.self, from: data)
            completionHandler(inspections, true, "")
        } catch {
            print("in catch \(error)")
            completionHandler(nil, false, error.localizedDescription)
        }
    }
    
    func postRequest(model: RealmQuestionAnswerModel) {
        let parameters: [String: Any] = [
            "id" : model.id,
            "questionnaire_global_id" : model.questionnaire_global_id,
            "questionnaire_global_id" : model.question_global_id,
            "question_type": model.question_type,
            "answer": model.answer ?? ""
        ]
        //AF.request("http://myserver.com", method:.post, parameters: parameters,encoding: JSONEncoding.default) .responseJSON { (response) in
          //  print(response)
         // Sync realm database to show the data has been posted.
        //}
    }
}
