//
//  Constants.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 19/05/2021.
//

import Foundation

import UIKit

public enum Constants {
    
    public enum Alerts {
        public static let loginErrorTitle = "Error"
        public static let loginErrorMessage = "Both email and password are mandatory."
    }
    
    public enum Segues {
        public static let toHomeVC = "homeVCSegue"
        public static let toTypeVC = "typeSegue"
        public static let toAreaVC = "areaSegue"
        public static let toInspectionVC = "inspectionSegue"
    }
    
    public enum CollectionViewsCells {
        public enum HomeScreen {
            public static let XibName = "HomeCollectionViewCell"
            public static let cellIdentifier = "homeCell"
        }
    }
    
    public enum TableViewCells {
        public enum TypeScreen {
            public static let cellIdentifier = "typeCell"
        }
        
        public enum AreaScreen {
            public static let cellIdentifier = "areaCell"
        }
        
        public enum InspectionScreen {
            public static let headerXibName = "InspectionQuestionHeaderTableViewCell"
            public static let headerCellIdentifier = "questionHeaderCell"
            
            public static let footerXibName = "InspectionQuestionFooterTableViewCell"
            public static let footerCellIdentifier = "questionFooterCell"
            
            public static let checkboxQuestionXibName = "CheckboxQuestionTableViewCell"
            public static let checkboxQuestionCellIdentifier = "checkboxQuestionOption"
            
            public static let textfieldQuestionXibName = "TextfieldQuestionTableViewCell"
            public static let textfieldQuestionCellIdentifier = "textfieldQuestionCell"
            
            public static let progressBarXibName = "ProgressBarQuestionTableViewCell"
            public static let progressBarCellIdentifier = "progressBarQuestionCell"
        }
    }
}
