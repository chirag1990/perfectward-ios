//
//  RealmQuestionAnswerModel.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import Foundation
import RealmSwift

public class RealmQuestionAnswerModel: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var questionnaire_global_id: String = ""
    @objc public dynamic var question_global_id: String = ""
    @objc public dynamic var question_type: String = ""
    @objc public dynamic var answer: String?
    @objc public dynamic var completedDateTime: Date? = Date()
    @objc public dynamic var isSaved: Bool = false
    @objc public dynamic var isSynced: Bool = false
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}
