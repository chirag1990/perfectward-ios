//
//  QuestionsnaireModel.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import Foundation
import RealmSwift

public class RealmQuestionnaireModel: Object {
    @objc public dynamic var id: String = ""
    @objc public dynamic var questionnaire_global_id: String = ""
    @objc public dynamic var isCompleted: Bool = false
    @objc public dynamic var isSynced: Bool = false
    @objc public dynamic var completedDateTime: Date? = Date()
    @objc public dynamic var isSaved: Bool = false
    @objc public dynamic var savedDate: Date?
    
    override public static func primaryKey() -> String? {
        return "id"
    }
}
