//
//  InspectionModel.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 20/05/2021.
//

import Foundation

// MARK: - InspectionModel
struct InspectionModel: Codable {
    let type: [TypeElement]
}

// MARK: - TypeElement
struct TypeElement: Codable {
    let name: String
    let area: [Area]
}

// MARK: - Area
struct Area: Codable {
    let name, inspectionGlobalID: String
    let inspection: [Inspection]

    enum CodingKeys: String, CodingKey {
        case name
        case inspectionGlobalID = "inspection_global_id"
        case inspection
    }
}

// MARK: - Inspection
struct Inspection: Codable {
    let questionNumber: Int
    let questionGlobalID, question, questionType: String
    let questionOptions: [QuestionOption]?
    let minValue, maxValue: Int?

    enum CodingKeys: String, CodingKey {
        case questionNumber = "question_number"
        case questionGlobalID = "question_global_id"
        case question, questionType, questionOptions, minValue, maxValue
    }
}

// MARK: - QuestionOption
struct QuestionOption: Codable {
    let options: String
}
