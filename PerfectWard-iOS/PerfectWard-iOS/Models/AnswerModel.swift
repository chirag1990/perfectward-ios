//
//  AnswerModel.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 24/05/2021.
//

import Foundation


public class AnswerModel {
    
    var answer: String?
    var questionType: String?
    var question_global_id: String?
    
}
