//
//  UsersModel.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 19/05/2021.
//

import Foundation


// MARK: - LoginModel
struct UserModel: Codable {
    let users: [User]
}

// MARK: - User
struct User: Codable {
    let email, password, name: String
}
