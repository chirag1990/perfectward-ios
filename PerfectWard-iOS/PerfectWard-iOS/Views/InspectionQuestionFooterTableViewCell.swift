//
//  InspectionQuestionFooterTableViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import UIKit

class InspectionQuestionFooterTableViewCell: UITableViewHeaderFooterView {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    var nextButtonPressed : (() -> ()) = {}
    var backButtonPressed : (() -> ()) = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.InspectionScreen.footerXibName, bundle: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        nextButtonPressed()
    }
    @IBAction func backButtonPressed(_ sender: Any) {
        backButtonPressed()
    }
    
}
