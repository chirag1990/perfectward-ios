//
//  InspectionQuestionHeaderTableViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 21/05/2021.
//

import UIKit

class InspectionQuestionHeaderTableViewCell: UITableViewHeaderFooterView {

    @IBOutlet weak var questionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.InspectionScreen.headerXibName, bundle: nil)
    }
}
