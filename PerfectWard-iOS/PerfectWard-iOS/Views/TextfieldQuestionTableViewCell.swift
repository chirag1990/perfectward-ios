//
//  TextfieldQuestionTableViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import UIKit

class TextfieldQuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var textfield: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.InspectionScreen.textfieldQuestionXibName, bundle: nil)
    }
    
}
