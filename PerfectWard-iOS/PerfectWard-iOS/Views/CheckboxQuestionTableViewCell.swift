//
//  CheckboxQuestionTableViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import UIKit

class CheckboxQuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var optionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.InspectionScreen.checkboxQuestionXibName, bundle: nil)
    }
    
}
