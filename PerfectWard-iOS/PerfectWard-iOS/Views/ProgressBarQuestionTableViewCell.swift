//
//  ProgressBarQuestionTableViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import UIKit

class ProgressBarQuestionTableViewCell: UITableViewCell {

    @IBOutlet weak var sliderBar: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.TableViewCells.InspectionScreen.progressBarXibName, bundle: nil)
    }
}
