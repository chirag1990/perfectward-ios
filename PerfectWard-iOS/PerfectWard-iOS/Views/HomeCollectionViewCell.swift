//
//  HomeCollectionViewCell.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 20/05/2021.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static func nib() -> UINib {
        return UINib(nibName: Constants.CollectionViewsCells.HomeScreen.XibName, bundle: nil)
    }
    
    func configureCell(cellName: String) {
        layer.borderWidth = 1
        layer.cornerRadius = 5
        layer.borderColor = UIColor.black.cgColor
        layer.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        nameLabel.text = cellName
    }

}
