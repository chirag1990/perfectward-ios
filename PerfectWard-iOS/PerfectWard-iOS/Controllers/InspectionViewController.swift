//
//  InspectionViewController.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 21/05/2021.
//

import UIKit

class InspectionViewController: UIViewController {
    
    var selectedArea: Area?
    var inspections: [Inspection]?
    
    var progressBarSlider: Int = 0
    var textFieldText: String = ""
    var optionsText: String = ""
    
    var fetchedAnswers: [RealmQuestionAnswerModel]?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        registerXibs()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let questionnaireId = self.selectedArea?.inspectionGlobalID {
            fetchedAnswers = DataConnector.shared.fetchSavedAnswersForQuestionnaire(questionnaireId: questionnaireId)
            tableView.reloadData()
        }
    }
    
    func registerXibs() {
        tableView.register(InspectionQuestionHeaderTableViewCell.nib(), forHeaderFooterViewReuseIdentifier: Constants.TableViewCells.InspectionScreen.headerCellIdentifier)
        tableView.register(InspectionQuestionFooterTableViewCell.nib(), forHeaderFooterViewReuseIdentifier: Constants.TableViewCells.InspectionScreen.footerCellIdentifier)
        tableView.register(CheckboxQuestionTableViewCell.nib(), forCellReuseIdentifier: Constants.TableViewCells.InspectionScreen.checkboxQuestionCellIdentifier)
        tableView.register(TextfieldQuestionTableViewCell.nib(), forCellReuseIdentifier: Constants.TableViewCells.InspectionScreen.textfieldQuestionCellIdentifier)
        tableView.register(ProgressBarQuestionTableViewCell.nib(), forCellReuseIdentifier: Constants.TableViewCells.InspectionScreen.progressBarCellIdentifier)
    }
    
    @objc func sliderChange(_ sender: UISlider) {
        progressBarSlider = (Int(sender.value))
    }
    
    @objc public func saveTapped() {

        var answerModelArray = [AnswerModel]()
        
        for inspection in self.inspections ?? [] {
            let answerModel = AnswerModel()
            
            if inspection.questionType == "checkbox" {
                answerModel.question_global_id = inspection.questionGlobalID
                answerModel.answer = optionsText
                answerModel.questionType = inspection.questionType
                answerModelArray.append(answerModel)
            }
            
            if inspection.questionType == "textfield" {
                answerModel.question_global_id = inspection.questionGlobalID
                answerModel.answer = textFieldText
                answerModel.questionType = inspection.questionType
                answerModelArray.append(answerModel)
            }
            
            if inspection.questionType == "progressBar" {
                answerModel.question_global_id = inspection.questionGlobalID
                answerModel.answer = String(progressBarSlider)
                answerModel.questionType = inspection.questionType
                answerModelArray.append(answerModel)
            }
        }
        
        if let questionnaireId = self.selectedArea?.inspectionGlobalID {
            DataConnector.shared.saveQuestionnaire(questionnaireId: questionnaireId)
            DataConnector.shared.saveAnswersForQuestionnaire(questionnaireId: questionnaireId, answerModel: answerModelArray)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveToDatabase() {
        let model = RealmQuestionAnswerModel()
        if fetchedAnswers != nil {
            for answers in fetchedAnswers ?? [] {
                model.id = answers.id
                model.question_global_id = answers.question_global_id
                model.questionnaire_global_id = answers.questionnaire_global_id
                model.answer = answers.answer
                model.question_type = answers.question_type
            }
        } else {
            saveTapped()
            if let questionnaireId = self.selectedArea?.inspectionGlobalID {
                fetchedAnswers = DataConnector.shared.fetchSavedAnswersForQuestionnaire(questionnaireId: questionnaireId)
                tableView.reloadData()
            }
            
            for answers in fetchedAnswers ?? [] {
                model.id = answers.id
                model.question_global_id = answers.question_global_id
                model.questionnaire_global_id = answers.questionnaire_global_id
                model.answer = answers.answer
                model.question_type = answers.question_type
            }
            
        }
        InspectionServices.shared.postRequest(model: model)
    }
    
}

// MARK: - UITableViewDelegate
extension InspectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CheckboxQuestionTableViewCell {
            optionsText = cell.optionLabel.text ?? ""
        }
    }
}

// MARK: - UITableViewDataSource
extension InspectionViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.inspections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // we have to workout what type question is it, so we can show correct number of rows.
        
        let inspectionQuestionType = self.inspections?[section].questionType
        
        switch inspectionQuestionType {
        case "checkbox":
            return self.inspections?[section].questionOptions?.count ?? 0
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let inspectionQuestionType = self.inspections?[indexPath.section].questionType
        
        
        
        switch inspectionQuestionType {
        case "checkbox":
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.InspectionScreen.checkboxQuestionCellIdentifier) as? CheckboxQuestionTableViewCell {
                cell.optionLabel.text = self.inspections?[indexPath.section].questionOptions?[indexPath.row].options
                
                if fetchedAnswers != nil {
                    for answers in fetchedAnswers ?? [] {
                        if answers.question_global_id == self.inspections?[indexPath.section].questionGlobalID {
                            if cell.optionLabel.text == answers.answer {
                                cell.setSelected(true, animated: false)
                                break
                            }
                        }
                    }
                }
                
                return cell
            }
        case "textfield":
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.InspectionScreen.textfieldQuestionCellIdentifier) as? TextfieldQuestionTableViewCell {
                
                cell.textfield.delegate = self
                
                if fetchedAnswers != nil {
                    for answers in fetchedAnswers ?? [] {
                        if answers.question_global_id == self.inspections?[indexPath.section].questionGlobalID {
                            cell.textfield.text = answers.answer
                        }
                    }
                }
                
                return cell
            }
        case "progressBar":
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.InspectionScreen.progressBarCellIdentifier) as? ProgressBarQuestionTableViewCell {
                cell.sliderBar.addTarget(self, action: #selector(sliderChange), for: .valueChanged)
                
                if fetchedAnswers != nil {
                    for answers in fetchedAnswers ?? [] {
                        if answers.question_global_id == self.inspections?[indexPath.section].questionGlobalID {
                            cell.sliderBar.value = answers.answer?.floatValue() ?? 0.0
                        }
                    }
                    
                }
                return cell
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.TableViewCells.InspectionScreen.headerCellIdentifier) as? InspectionQuestionHeaderTableViewCell {
            
            headerView.questionLabel.text = self.inspections?[section].question
            
            return headerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let footerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.TableViewCells.InspectionScreen.footerCellIdentifier) as? InspectionQuestionFooterTableViewCell {
            
            footerView.nextButtonPressed = {
                self.saveToDatabase()
            }
            
            footerView.backButtonPressed = {
                self.navigationController?.popViewController(animated: true)
            }
            
            return footerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if section == tableView.numberOfSections - 1 {
            return 100
        } else {
            return 0
        }
    }
}

// MARK: - UITextFieldDelegate
extension InspectionViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldText = textField.text ?? ""
    }
}
