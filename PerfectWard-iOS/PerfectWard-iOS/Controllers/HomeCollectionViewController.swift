//
//  HomeCollectionViewController.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 19/05/2021.
//

import UIKit

class HomeCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var welcomeLabel: UILabel!
    var user: User?
    
    var homeCellsArray = ["Inspections", "Historic Inspections", "Future Inspections"]
    var vcTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(HomeCollectionViewCell.nib(), forCellWithReuseIdentifier: Constants.CollectionViewsCells.HomeScreen.cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.title = "Inspections"
        self.view.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        self.collectionView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        
        let name = user?.name ?? "Guest"
        welcomeLabel.text = "Welcome \(name)"
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toTypeVC {
            if let inspectionVC = segue.destination as? TypeListViewController {
                inspectionVC.vcTitle = vcTitle
            }
        }
    }
}

extension HomeCollectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeCellsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CollectionViewsCells.HomeScreen.cellIdentifier, for: indexPath) as? HomeCollectionViewCell {
            cell.configureCell(cellName: homeCellsArray[indexPath.row])
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension HomeCollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            vcTitle = homeCellsArray[indexPath.row]

            performSegue(withIdentifier: Constants.Segues.toTypeVC, sender: nil)
        }
    }
}

extension HomeCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.frame.size.width - 20)/2, height: (self.collectionView.frame.size.height) / 3)
    }
    
}
