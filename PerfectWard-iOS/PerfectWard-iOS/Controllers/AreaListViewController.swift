//
//  AreaListViewController.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 21/05/2021.
//

import UIKit

class AreaListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var areaForSelectedType: [Area]?
    var selectedArea: Area?
    var inspections: [Inspection]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select Area"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toInspectionVC {
            if let inspectionVC = segue.destination as? InspectionViewController {
                inspectionVC.selectedArea = selectedArea
                inspectionVC.inspections = inspections
            }
        }
    }
}

extension AreaListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedArea = self.areaForSelectedType?[indexPath.row]
        inspections = self.areaForSelectedType?[indexPath.row].inspection
        performSegue(withIdentifier: Constants.Segues.toInspectionVC, sender: nil)
    }
}

extension AreaListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return areaForSelectedType?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.AreaScreen.cellIdentifier) {
            cell.textLabel?.text = areaForSelectedType?[indexPath.row].name ?? ""
            return cell
        }
        
        return UITableViewCell()
    }
}
