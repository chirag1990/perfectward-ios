//
//  InspectionViewController.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 20/05/2021.
//

import UIKit

class TypeListViewController: UIViewController {
    
    var vcTitle: String = ""
    @IBOutlet weak var tableView: UITableView!
    var inspections: InspectionModel?
    var selectedTypeForArea: [Area]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = vcTitle
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()

        self.title = "Select Type"
        
        InspectionServices.shared.getInspections { (inspections, status, message) in
            if status {
                self.inspections = inspections
            } else {
                self.showAlert(withTitle: "Found Error", message: message)
            }
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toAreaVC {
            if let areaVC = segue.destination as? AreaListViewController {
                areaVC.areaForSelectedType = selectedTypeForArea
            }
        }
    }
}

extension TypeListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedTypeForArea = self.inspections?.type[indexPath.row].area
        performSegue(withIdentifier: Constants.Segues.toAreaVC, sender: nil)
    }
}


extension TypeListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inspections?.type.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableViewCells.TypeScreen.cellIdentifier) {
            cell.textLabel?.text = inspections?.type[indexPath.row].name
            return cell
        }
        
        return UITableViewCell()
    }
}
