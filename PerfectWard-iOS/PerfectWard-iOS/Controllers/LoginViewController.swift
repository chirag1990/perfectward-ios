//
//  ViewController.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 19/05/2021.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        
        if let email = emailTextField.text, let password = passwordTextField.text {
            // check if password is empty and email has to be valid
            if password.isEmpty && !email.isEmailValid() {
                self.showAlert(withTitle: Constants.Alerts.loginErrorTitle, message: Constants.Alerts.loginErrorMessage)
            } else {
                // if not empty performSegue
                AuthServices.shared.logIn(with: email, password: password) { (user, success, message) in
                    if success {
                        guard let user = user else {
                            print("unable to unwrap user")
                            return
                        }
                        self.user = user
                        self.performSegue(withIdentifier: Constants.Segues.toHomeVC, sender: nil)
                    } else {
                        self.showAlert(withTitle: "Login Failed", message: message)
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.toHomeVC {
            if let navVC = segue.destination as? UINavigationController {
                if let homeVC = navVC.viewControllers.first as? HomeCollectionViewController {
                    homeVC.user = self.user
                }
            }
        }
    }
    
    func setUpValidationBorder(for view : UIView, color : CGColor, borderWidth : CGFloat = 2) {
        view.layer.borderWidth = borderWidth
        view.layer.borderColor = color
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTextField:
            self.passwordTextField.becomeFirstResponder()
        default:
            self.passwordTextField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            if let email = emailTextField.text, !email.isEmailValid() {
                setUpValidationBorder(for: emailTextField, color: UIColor.red.cgColor)
            } else {
                setUpValidationBorder(for: emailTextField, color: UIColor.blue.cgColor)
            }
        }
        
        if textField == passwordTextField {
            if let password = passwordTextField.text, password.isEmpty {
                setUpValidationBorder(for: passwordTextField, color: UIColor.red.cgColor)
            } else {
                setUpValidationBorder(for: passwordTextField, color: UIColor.blue.cgColor)
            }
        }
    }
}

