//
//  DataConnector.swift
//  PerfectWard-iOS
//
//  Created by Chirag Tailor on 23/05/2021.
//

import RealmSwift


class DataConnector {
    
    public static var shared = DataConnector()
    
    public func saveQuestionnaire(questionnaireId: String) {
        
        let value = Array(RealmConnector.shared.fetchSavedQuestionnaireFromRealm(questionnaireId: questionnaireId))
        
        if value.isEmpty {
            let id = UUID().uuidString
            RealmConnector.shared.writeQuestionnaireToRealm(with: id, questionnaireId: questionnaireId)
        }
    }
    
    public func saveAnswersForQuestionnaire(questionnaireId: String, answerModel: [AnswerModel] ) {
        
        let realmAnswerModels = Array(RealmConnector.shared.fetchSavedAnswer(questionnaireId: questionnaireId))
        
        if realmAnswerModels.isEmpty {
            
            for answers in answerModel {
                let id = UUID().uuidString
                RealmConnector.shared.writeAnswersForQuestionnaires(questionnaireId: questionnaireId, primaryKey: id, answerModel: answers)
            }
        } else {
            for answerData in answerModel {
                let model = Array(RealmConnector.shared.fectchSavedAnswerForQuestionGlobalId(questionGlobalId: answerData.question_global_id ?? ""))
                
                if !model.isEmpty {
                    let id = model.first?.id ?? ""
                    RealmConnector.shared.writeAnswersForQuestionnaires(questionnaireId: questionnaireId, primaryKey:id, answerModel: answerData)
                    
                }
            }
        }
    }
    
    public func fetchSavedAnswersForQuestionnaire(questionnaireId: String) -> [RealmQuestionAnswerModel] {
        let result = Array(RealmConnector.shared.fetchSavedAnswer(questionnaireId: questionnaireId))
        return result
    }
}
